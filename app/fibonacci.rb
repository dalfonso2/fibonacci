module FiboMejorado

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    
    def method_added method_name
      if @metodos_a_cachear.include?(method_name)
        @metodos_a_cachear.delete method_name
        priv_method = "_#{method_name}_"
        alias_method priv_method, method_name
        define_method method_name do |arg|
          @valores_cacheados ||= {}
          @valores_cacheados[[method_name, arg]] ||= (send priv_method, arg)
        end
      end
    end
    
    def cachear_resultado (method_name)
      @metodos_a_cachear ||= []
      @metodos_a_cachear << method_name
    end
  end
end

class Fibonacci
  include FiboMejorado
  cachear_resultado :at_position
  cachear_resultado :pepito

  def at_position(pos)
    return pos if pos < 2
    at_position(pos - 1) + at_position(pos - 2)
  end

  def pepito(entero)
    5
  end
end
