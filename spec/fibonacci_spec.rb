require 'spec_helper'
require 'timeout'

require File.dirname(__FILE__) + '/../app/fibonacci'

describe 'fibonacci' do

  let(:fibonacci) { Fibonacci.new }

  it 'at position 0 should return 0' do

    result = fibonacci.at_position(0)

    expect(result).to eq(0)
  end

  it 'at position 1 should return 1' do

    result = fibonacci.at_position(1)

    expect(result).to eq(1)
  end

  it 'at position 2 should return 1' do

    result = fibonacci.at_position(2)

    expect(result).to eq(1)
  end

  it 'at position 3 should return 2' do

    result = fibonacci.at_position(3)

    expect(result).to eq(2)
  end

  it 'at position 5 should return 5' do

    result = fibonacci.at_position(5)

    expect(result).to eq(5)
  end

  it 'at position 8 should return 21' do

    result = fibonacci.at_position(8)

    expect(result).to eq(21)
  end

  it 'at position 50 should return 12586269025 without timing out' do
    big_fibonacci = lambda do
        Timeout::timeout(5) do # 20 seconds
        result = fibonacci.at_position(50)
        expect(result).to eq(12586269025)
      end
    end
    expect { big_fibonacci.call }.to_not raise_error(Timeout::Error)
  end

end
